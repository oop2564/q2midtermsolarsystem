/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.q2midtermsolarsystem;

/**
 *
 * @author Administrator
 */
public class Stars extends SolarSystem {

    //Constructor
    public Stars(String name, String OtherName) {
        super(name, OtherName);
        System.out.println("Stars created");
        System.out.println("Name: " + this.name + " --- Other Name: " + this.OtherName);
    }
    
    //เช็คว่ามีแสงสว่างในตัวเองมั้ย
    @Override
    public void Ownlight() {
        System.out.println(this.name + " have ownlight");
    }
    
    //เช็คประเภทของดาวเคราะห์นั้นๆหรือดาวฤกษ์นั้นๆ
    @Override
    public void type() {
        System.out.println("Type of " + this.name + " is Yellow Dwarf Star");
    }
}
