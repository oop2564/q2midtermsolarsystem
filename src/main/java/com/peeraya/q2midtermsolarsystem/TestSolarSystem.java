/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.q2midtermsolarsystem;

/**
 *
 * @author Administrator
 */
public class TestSolarSystem {
    
    public static void main(String[] args) {
        SolarSystem ss = new SolarSystem("Solar System", "-");
        ss.Ownlight();
        ss.type();
        ss.moon();
        ss.ring();
        ss.BreakRange();
        
        Stars sun = new Stars("Sun", "-");
        sun.Ownlight();
        sun.type();
        sun.moon();
        sun.ring();
        sun.BreakRange();

        Planets mercury = new Planets("Mercury", "Frozen Fireplace");
        mercury.Ownlight();
        mercury.InnerOROuterPlanets("Mercury");
        mercury.type();
        mercury.moon();
        mercury.ring();
        mercury.setTimeItselfnSun(56.646, 88);
        mercury.BreakRange();

        Planets venus = new Planets("Venus", "Earth's twin");
        venus.Ownlight();
        venus.InnerOROuterPlanets("Venus");
        venus.type();
        venus.moon();
        venus.ring();
        venus.setTimeItselfnSun(243, 225);
        venus.BreakRange();

        Planets earth = new Planets("Earth", "The Blue Planets");
        earth.Ownlight();
        earth.InnerOROuterPlanets("Earth");
        earth.type();
        earth.moon(1);
        earth.ring();
        earth.setTimeItselfnSun(24, 365);
        earth.BreakRange();

        Planets mars = new Planets("Mars", "The Red Planets");
        mars.Ownlight();
        mars.InnerOROuterPlanets("Mars");
        mars.type();
        mars.moon(2);
        mars.ring();
        mars.setTimeItselfnSun(24.623, 687);
        mars.BreakRange();

        Planets jupiter = new Planets("Jupiter", "Giant Planets");
        jupiter.Ownlight();
        jupiter.InnerOROuterPlanets("Jupiter");
        jupiter.type();
        jupiter.moon(79);
        jupiter.ring("Jupiter");
        jupiter.setTimeItselfnSun(9.925, 12);
        jupiter.BreakRange();

        Planets saturn = new Planets("Saturn", "God of Agriculture");
        saturn.Ownlight();
        saturn.InnerOROuterPlanets("Saturn");
        saturn.type();
        saturn.moon(82);
        saturn.ring("Saturn");
        saturn.setTimeItselfnSun(10.42, 29);
        saturn.BreakRange();

        Planets uranus = new Planets("Uranus", "Sideways Planets");
        uranus.Ownlight();
        uranus.InnerOROuterPlanets("Uranus");
        uranus.type();
        uranus.moon(27);
        uranus.ring("Uranus");
        uranus.setTimeItselfnSun(17.24, 84);
        uranus.BreakRange();

        Planets neptune = new Planets("Neptune", "Ice Giant Planets");
        neptune.Ownlight();
        neptune.InnerOROuterPlanets("Neptune");
        neptune.type();
        neptune.moon(14);
        neptune.ring("Neptune");
        neptune.setTimeItselfnSun(16.06, 165);
        neptune.BreakRange();
    }
}
