/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.q2midtermsolarsystem;

/**
 *
 * @author Administrator
 */
public class Planets extends SolarSystem {
    
    private double timearounditself;
    private double timearoundsun;

    //Constructor
    public Planets(String name, String OtherName) {
        super(name, OtherName);
        System.out.println("Planets created");
        System.out.println("Name: " + this.name + " --- Other Name: " + this.OtherName);
    }
    
    //เช็คว่ามีแสงสว่างในตัวเองมั้ย
    @Override
    public void Ownlight() {
        System.out.println(this.name + " don't have ownlight");
    }
    
    //เช็คประเภทของดาวเคราะห์นั้นๆหรือดาวฤกษ์นั้นๆ
    @Override
    public void type() {
        switch(name) {
            case "Mercury": 
            case "Venus":
            case "Earth":
            case "Mars":
                System.out.println("Type of " + this.name + " is Rocky Planets");
                break;
            case "Jupiter": 
            case "Saturn":
                System.out.println("Type of " + this.name + " is Gas Giant");
                break;
            case "Uranus":
            case "Neptune":
                System.out.println("Type of " + this.name + " is Ice Giant");
                break;
        }
    }
    
    //ตรวจสอบว่าเป็นดาวเคราะห์ชั้นในหรือดาวเคราะห์ชั้นนอก
    public boolean InnerOROuterPlanets(String name) {
        switch(name) {
            case "Mercury": 
            case "Venus":
            case "Earth":
            case "Mars":
                System.out.println(this.name + " is Inner Planets");
                break;
            case "Jupiter": 
            case "Saturn":
            case "Uranus":
            case "Neptune":
                System.out.println(this.name + " is Outer Planets");
                break;
        }
        return false;
    }
    
    //เพิ่มการ getter and setter
    //บอกเวลาที่ดาวนั้นๆหมุนรอบตัวเอง
    public double getTimeItself() {
        return timearounditself;
    }
    //บอกเวลาเวลาที่ดาวนั้นๆโคจรรอบดวงอาทิตย์
    public double getTimeSun() {
        return timearoundsun;
    }
    //กำหนดค่าเวลาที่ดาวนั้นๆหมุนรอบตัวเองและเวลาที่ดาวนั้นๆโคจรรอบดวงอาทิตย์
    public void setTimeItselfnSun(double timearounditself, double timearoundsun) {
        this.timearounditself = timearounditself;
        this.timearoundsun = timearoundsun;
        switch(name) {
            case "Mercury": 
            case "Venus":
                System.out.println("The period of rotation on " + this.name + " is " + this.getTimeItself() + " Earth days");
                System.out.println("The period of revolution on " + this.name + " is " + this.getTimeSun() + " Earth days");
                break;
            case "Earth":
            case "Mars":
                System.out.println("The period of rotation on " + this.name + " is " + this.timearounditself + " Earth hours");
                System.out.println("The period of revolution on " + this.name + " is " + this.timearoundsun + " Earth days");
                break;
            case "Jupiter": 
            case "Saturn":
            case "Uranus":
            case "Neptune":
                System.out.println("The period of rotation on " + this.name + " is " + this.timearounditself + " Earth hours");
                System.out.println("The period of revolution on " + this.name + " is " + this.timearoundsun + " Earth years");
                break;
        }
    }
}
