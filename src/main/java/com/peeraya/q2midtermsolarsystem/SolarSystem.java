/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.q2midtermsolarsystem;

/**
 *
 * @author Administrator
 */
public class SolarSystem {

    protected String name;
    protected String OtherName;
    private int x = 0;

    //Constructor
    public SolarSystem(String name, String OtherName) {
        this.name = name;
        this.OtherName = OtherName;
        System.out.println("Solar System created");
    }
    
    //เช็คว่ามีแสงสว่างในตัวเองมั้ย
    public void Ownlight() {
        System.out.print("");
    }
    
    //เช็คประเภทของดาวเคราะห์นั้นๆหรือดาวฤกษ์นั้นๆ
    public void type() {
        System.out.print("");
    }
    
    //Overloading
    //ตรวจสอบดาวบริวาร(ดวงจันทร์) --- ในกรณีที่ไม่มี
    public void moon() {
        System.out.println(this.name + " has no moon : Moonless");
    }
    //ตรวจสอบดาวบริวาร(ดวงจันทร์) --- ในกรณีที่่มี
    public void moon(int x) {
        this.x = x;
        if (x < 2) {
            System.out.println(this.name + " has " + this.x + " moon");
            return;
        } else {
            System.out.println(this.name + " has " + this.x + " moons");
        }
    }
    
    //Overloading
    //ตรวจสอบว่ามีวงแหวนหรือไม่ --- ในกรณีที่ไม่มี
    public void ring() {
        System.out.println(this.name + " has no rings : Ringless");
    }
    //ตรวจสอบว่ามีวงแหวนหรือไม่ --- ในกรณีที่่มี
    public void ring(String name) {
        switch (name) {
            case "Jupiter":
                System.out.println("Ring of " + this.name + " is Ringed world");
                break;
            case "Saturn":
                System.out.println("Ring of " + this.name + " is Glorious rings");
                break;
            case "Uranus":
                System.out.println("Ring of " + this.name + " is The other ringed world");
                break;
            case "Neptune":
                System.out.println("Ring of " + this.name + " is Faint rings");
                break;
        }
    }
    
    //เส้นคั่น ไว้มองแยกช่วง
    public void BreakRange() {
        System.out.println("-------------------------");
    }
}
